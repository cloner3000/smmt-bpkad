import React, { Component } from 'react';
import {
  Route,
  Switch,
  Redirect
} from "react-router-dom";
import cookie from 'react-cookies';

import Header from './components/Header'
import Sidebar from './components/Sidebar'
import Home from './components/Home'
import NewTask from './components/NewTask'
import Detail from './components/Detail'
import Register from './components/Register'
import Login from './components/Login'
import Profile from './components/Profile'
import TaskSent from './components/TaskSent'
import TaskReceive from './components/TaskReceive'
import Welcome from './components/Welcome'
import Users from './components/Users'
import Tasks from './components/Tasks'
import Administrator from './components/Administrator'


class App extends Component {

  constructor(props) {
    super(props);
  
    this.state = {
      role : 0
    };
  }

  componentWillMount(){
    if (cookie.load('token') === undefined && cookie.load('access') !== undefined) {
      this.setState({
        role : 1
      })
      
    }else if(cookie.load('token') !== undefined && cookie.load('access') !== undefined) {
      this.setState({
        role : 2
      })
    }
    else if(cookie.load('access') === undefined && cookie.load('token') === undefined) {
      this.setState({
        role : 0
      })
    }
  }

  render() {
    const {role} = this.state
    let content = null
    if (role == 2) {

      content = 
      <Switch>
        <div id="wrapper" class="wrapper">
          <Header/>
          <div class="content-wrapper">
            <Sidebar/>
            <Route exact path="/" component={Home}/>
            <Route exact path="/detail/:name" component={Detail}/>
            <Route exact path="/profile" component={Profile}/>
            <Route exact path="/profile/:name" component={Profile}/>
            <Route exact path="/create" component={NewTask}/>
            <Route exact path="/sent" component={TaskSent}/>
            <Route exact path="/receive" component={TaskReceive}/>
            <Route exact path="/manageuser" component={Users}/>
            <Route exact path="/managetask" component={Tasks}/>
            <Route exact path="/administrator" component={Administrator}/>
            <Redirect to="/" />
          </div>
        </div>    
      </Switch>

    } else if(role == 1) {
      content = 
      <Switch>
        <div class="body-bg-full profile-page" style={{'backgroundImage' : 'url(./assets/img/bg1.jpg)'}}>
          <Route exact path="/" component={Login}/>
          <Route exact path="/daftar" component={Register}/>
          <Redirect to="/" />
        </div>
        
      </Switch>
    }else if(role == 0) {
      content = 
      <Switch>
        <div class="body-bg-full profile-page" style={{'backgroundImage' : 'url(./assets/img/bg1.jpg)'}}>
          <Route exact path="/" component={Welcome}/>
          <Redirect to="/" />
        </div>
        
      </Switch>
    }

    return (
      
      <div>
        {content}
      </div>

    );
  }
}

export default App;
