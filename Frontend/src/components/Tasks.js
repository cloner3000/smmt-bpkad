import { Scrollbars } from 'react-custom-scrollbars';
import React, { Component } from 'react';
import {
  Route,
  Switch
} from "react-router-dom";
import axios from 'axios'
import {NavLink} from "react-router-dom";
import '../animate.css';
import {Animated} from "react-animated-css";
import cookie from 'react-cookies';

class Tasks extends Component {

    constructor(props){
        super(props)
        this.state = {
            task    : [],
            id      : '',
            judul   : '',
            pemilik : '',
            success : false,
            length  : 0
        }
    }

    handleGetData(id, judul, nama){
        this.setState({
            id, judul, pemilik : nama
        })
    }

    componentDidMount(){
        const self = this
        axios.get('http://localhost:8000/api/task')
        .then(function (response) {
            let task = Object.keys(response.data).length
            self.setState({task : response.data, length : task})
        })
        .catch(function (error) {
            console.log(error);
        });
    }

    handleDeleteTask = () => {
        const self = this
        axios.delete('http://localhost:8000/api/task/' + self.state.id)
        .then(function (response) {
            let task = Object.keys(response.data).length
            self.setState({task : response.data, success : true, length : task})
        })
        .catch(function (error) {
            console.log(error);
        });
    }


  render() {
      let index = 0
    return (

        <main class="main-wrapper clearfix">

            <div class="row page-title clearfix">
                <div class="page-title-left">
                    <h6 class="page-title-heading mr-0 mr-r-5">Kelola Tugas</h6>
                    <p class="page-title-description mr-0 d-none d-md-inline-block">[ Total : {this.state.length} Tugas ]</p>
                </div>
            </div>

            <br/>

            {
                this.state.success ?
                <Animated animationIn="rubberBand" animationOut="pulse" isVisible={this.state.success}>
                <div class="alert alert-icon alert-success border-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button> <i class="material-icons list-icon">check_circle</i>  <strong>Sukses!</strong> Tugas berhasil dihapus.</div>
                </Animated>
                : null
            }
            
            <br/>

            <div class="widget-list row">
                <div class="widget-holder widget-full-height col-md-12">
                    <div class="widget-bg">

                        <div class="widget-body">

                        <div class="widget-list">
                            <div class="row">
                                <div class="col-md-12 widget-holder">
                                    <div class="widget-bg">
                                        <div class="widget-body clearfix">
                                            <Scrollbars style={{ width: '100%', height: 400 }}>
                                                <table class="table table-editable table-responsive">
                                                    <thead>
                                                        <tr>
                                                            <th>No.</th>
                                                            <th>Judul</th>
                                                            <th>Pemberi Tugas</th>
                                                            <th>Status</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {
                                                            
                                                            this.state.task.map((tugas) => {
                                                                index += 1
                                                                return(
                                                                    <tr>
                                                                        <td>{index}</td>
                                                                        <td>
                                                                            <NavLink  to={"detail/" + tugas.id_task} >
                                                                                {tugas.judul}
                                                                            </NavLink>
                                                                        </td>
                                                                        <td>{tugas.nama}</td>
                                                                        <td>{tugas.st_judul}</td>
                                                                        <td><a onClick={() => this.handleGetData(tugas.id_task, tugas.judul, tugas.nama)} href="#" data-toggle="modal" data-target=".bs-modal-md" class="mr-3 btn btn-outline-danger pull-right">Hapus</a></td>
                                                                    </tr>
                                                                )
                                                            })
                                                        }
                                                    </tbody>

                                                </table>
                                            </Scrollbars>

                                            <div class="modal modal-danger fade bs-modal-md" tabindex="-1" role="dialog" aria-labelledby="myMediumModalLabel" aria-hidden="true" style={{"display": "none"}}>
                                            <div class="modal-dialog modal-md">
                                                <div class="modal-content">
                                                    <div class="modal-header text-inverse">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                        <h5 class="modal-title" id="myMediumModalLabel">Hapus Tugas</h5>
                                                    </div>
                                                    <div class="modal-body">
                                                        <h5>Apakah anda yakin ingin menghapus tugas ini ?</h5>
                                                        <p> Judul Tugas : {this.state.judul}</p>
                                                        <p> Pembuat Tugas : {this.state.pemilik}</p>
                                                    </div>
                                                    <div class="modal-footer"><a href="#" data-dismiss="modal" class="btn btn-info btn-rounded ripple text-left">Batal</a> 
                                                        <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal" onClick={this.handleDeleteTask}>Hapus</button>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            </div>
                                           
                                            
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                                
                            </div>
                            
                        </div>
                            
                        </div>
                        
                    </div>
                    
                </div>
                
                
                
            </div>
            
      </main>

    );
  }
}

export default Tasks;
