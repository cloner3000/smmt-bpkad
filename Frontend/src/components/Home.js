import React, { Component } from 'react';
import {
    NavLink
  } from "react-router-dom";
import axios from 'axios'
import { ClipLoader, BeatLoaders, GridLoader } from 'react-spinners'
import Loading from 'react-loading-bar'
import 'react-loading-bar/dist/index.css'
import cookie from 'react-cookies';


let limit = 3

class Home extends Component {

    constructor(props) {
        super(props);
      
        this.state = {
          show      : false,
          task      : [],
          loading   : true,
          load      : false
        };
      }
    
    componentDidMount(){
        const self = this
        //Loading bar
        setTimeout(() => {
            this.setState({show:true})
            setTimeout(() => {
                this.setState({show:false})
            }, 200)
        }, 100)
        
        //load task
        axios.get('http://localhost:8000/api/task/' + limit)
        .then(function (response) {
            self.setState({
                task    : response.data,
                loading : false
            })
        })
        .catch(function (error) {
            console.log(error);
        });

    }
    
    loadMore = () => {
        limit += 3
        const self = this
        //load task
        self.setState({
            load    : true
        })
        axios.get('http://localhost:8000/api/task/' + limit)
        .then(function (response) {
            console.log(response.data)
            self.setState({
                task    : response.data,
                load    : false
            })
        })
        .catch(function (error) {
            console.log(error);
        });
    }
    

  render() {
    return (
      
      <main class="main-wrapper clearfix">
        <Loading
          show={this.state.show}
          color="purple"
        />

        <div class="row page-title clearfix">
            <div class="page-title-left">
                <h6 class="page-title-heading mr-0 mr-r-5">Daftar Tugas BPKAD</h6>
                <p class="page-title-description mr-0 d-none d-md-inline-block">Judul & Status Tugas</p>
            </div>
        </div>

        <div class="widget-list row">
                <div class="widget-holder widget-full-height col-md-12">
                    <div class="widget-bg">
                        
                        <div class="widget-body">
                            {
                                this.state.loading ? 
                                <div style={{'marginLeft' : '45%'}}>
                                    <GridLoader
                                        color={'#408FE7'} 
                                        loading={this.state.loading}
                                    />
                                </div>
                                :
                                <table class="widget-latest-transactions">
                                    
                                    {
                                        this.state.task.map(function(user){
                                            let status = ''
                                            if(user.st_judul == "Belum dikerjakan") {
                                                status = "material-icons fs-18 color-danger"
                                            }
                                            else if(user.st_judul == "Sedang dikerjakan") {
                                                status = "material-icons fs-18 color-warning"
                                            }else {
                                                status = "material-icons fs-18 color-success"
                                            }

                                            return(
                                                
                                                <tr>
                                                    <td class="thumb-xs2" style={{'padding' : '0px'}}>
                                                        <a href="#">
                                                            <img class="rounded-circle" src={user.foto}  />
                                                        </a>
                                                    </td>
                                                    
                                                    <td class="single-user-details">
                                                        <NavLink  to={"detail/" + user.id} >
                                                            <a class="single-user-name" href="#">{user.nama}</a>  
                                                            <small>{user.judul}</small>
                                                        </NavLink>
                                                    </td>
                                                    
                                                    
                                                    <td class="single-status"><i class={status}>fiber_manual_record</i>  <span class="text-muted">{user.st_judul}</span>
                                                    <td class="single-amount"><small>{user.created_at}</small></td>
                                                    </td>
                                                </tr>
                                                
                                            )

                                        })
                                    }

                                </table>
                            }
                            
                        </div>
                        
                    </div>
                    
                </div>
                <button onClick={this.loadMore} style={{'margin' : '0 auto', 'display' : 'block'}} class="load-more-btn btn btn-primary">
                    {
                        this.state.load ?
                        <span>Memuat tugas ...</span>
                        :
                        <span>Lebih banyak</span>
                    }
                </button>
                <br/>
            </div>
            
      </main>

    );
  }
}

export default Home;