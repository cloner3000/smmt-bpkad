import React, { Component } from 'react';
import {NavLink} from "react-router-dom";
import axios from 'axios'
import cookie from 'react-cookies';

class Sidebar extends Component {

    constructor(props){

        super(props)

        this.state = {
            user : [],
            role : false
        }

    }

    componentWillMount(){
        if(cookie.load('role') > 0){
            this.setState({role : true})
        }
    }

    componentDidMount(){
        const self = this
        //Get Data Profile
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + cookie.load('token')

        axios.get('http://localhost:8000/api/profile')
        .then(function (response) {
            self.setState({user : response.data})
        })
        .catch(function (error) {
            console.log(error);
        });

    }

  render() {
      console.log("ANJING KAU " + this.state.user['foto'])
    return (
      
      <aside class="site-sidebar scrollbar-enabled" data-suppress-scroll-x="true">
       
            <div class="side-user">
                <div class="col-sm-12 text-center p-0 clearfix">
                    <div class="d-inline-block pos-relative mr-b-10">
                        <figure class="thumb-sm mr-b-0 user--online">
                            <img src={this.state.user['foto']} style={{'width' : 60, 'height' : 60}}  class="rounded-circle" alt="" />
                        </figure><a href="#" class="text-muted side-user-link"><i class="feather feather-settings list-icon"></i></a>
                    </div>
                    
                    <div class="lh-14 mr-t-5"><a href="#" class="hide-menu mt-3 mb-0 side-user-heading fw-500">{this.state.user['nama']}</a>
                        <br /><small class="hide-menu">{this.state.user['jabatan']}</small>
                    </div>
                </div>
                
            </div>
            

            <nav class="sidebar-nav">
                <ul class="nav in side-menu">
                        <li class="current-page menu-item-has-children">
                            <a>
                                <NavLink style={{'padding' : '0px'}} to="/">
                                <i class="list-icon fa fa-home"></i> 
                                <span class="hide-menu">Beranda</span>
                                </NavLink>
                            </a>
                        </li>
                    
                        <li class="menu-item-has-children active">
                            <a>
                                <NavLink style={{'padding' : '0px'}} to="/create">
                                    <i class="list-icon fa fa-pencil"></i> <span class="hide-menu">Buat Tugas</span>
                                </NavLink>
                            </a>
                        </li>
                    {
                        this.state.role ? null :
                        <li class="menu-item-has-children">
                            <a>
                            <NavLink style={{'padding' : '0px'}} to="/receive">
                                <i class="list-icon fa fa-tasks"></i> <span class="hide-menu">Tugas Masuk</span>
                            </NavLink>
                            </a>
                        </li>
                    }
                    <li class="menu-item-has-children">
                        <a>
                        <NavLink style={{'padding' : '0px'}} to="/sent">
                            <i class="list-icon fa fa-paper-plane"></i> <span class="hide-menu">Tugas Keluar</span>
                        </NavLink>
                        </a>
                    </li>
                    {
                        this.state.role ?
                            <li class="menu-item-has-children">
                                <a>
                                <NavLink style={{'padding' : '0px'}} to="/managetask">
                                    <i class="list-icon fa fa-file-text"></i> <span class="hide-menu">Kelola Tugas</span>
                                </NavLink>
                                </a>
                            </li>
                        : null
                    }
                    {
                        this.state.role ?
                            <li class="menu-item-has-children">
                                <a>
                                <NavLink style={{'padding' : '0px'}} to="/manageuser">
                                    <i class="list-icon fa fa-users"></i> <span class="hide-menu">Kelola Pengguna</span>
                                </NavLink>
                                </a>
                            </li>
                        : null
                    }
                    {
                        this.state.role ?
                            <li class="menu-item-has-children">
                                <a>
                                <NavLink style={{'padding' : '0px'}} to="/administrator">
                                    <i class="list-icon fa fa-wrench"></i> <span class="hide-menu">Pengaturan</span>
                                </NavLink>
                                </a>
                            </li>
                        : null
                    }
                </ul>
                
            </nav>
            
        </aside>

    );
  }
}

export default Sidebar;
