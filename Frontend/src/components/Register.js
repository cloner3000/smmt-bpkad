import React, { Component } from 'react';
import {NavLink} from "react-router-dom";
import cookie from 'react-cookies';
import '../animate.css';
import {Animated} from "react-animated-css";
import { PropagateLoader } from 'react-spinners';

import axios from 'axios'

class Register extends Component {

    constructor(props){

        super(props)
        this.state ={
            nip     : '',
            nama    : '',
            jabatan : '',
            selectedFile : null,
            password : '',
            loading : false,
            text    : false,
            success : false,
            error   : false
        }
    }
    handleNIP = event => { this.setState({nip : event.target.value}) }
    handleNama = event => { this.setState({nama : event.target.value}) }
    handleJabatan = event => { this.setState({jabatan : event.target.value}) }
    handleSandi = event => { this.setState({password : event.target.value}) }
    fileSelectedHandler = event => {
        this.setState({
          selectedFile : event.target.files[0]
        })
    }

    fileUploadHandler = (event) => {
        const self = this;

        self.setState({
            loading : true,
            text : true,
            error : false
        })

        const fd = new FormData()
        fd.append('image', this.state.selectedFile)
        axios.defaults.headers.common['Authorization'] = 'Client-ID af3942dc3f3cd90'

        axios.post('http://localhost:8000/api/auth/signupimage', fd)
        .then(function (response) {
            console.log('masuk brooo')
            //console.log(self.state.nama + self.state.nip + self.state.jabatan + self.state.password)
            console.log(response.data.name);
            self.registerUser(response.data.name)
        })
        .catch(function (error) {
            self.setState({
                error : true,
                success : false,
                loading : false,
                text : false
            })
        });
        event.preventDefault()
    }

    registerUser = (foto) => {
        const self = this
        //console.log(self.state.nama + self.state.nip + self.state.jabatan + self.state.password)
        axios.post('http://localhost:8000/api/auth/signup', {
            nip: self.state.nip,
            nama: self.state.nama,
            jabatan: self.state.jabatan,
            foto: foto,
            password: self.state.password,
          })
          .then(function (response) {
            self.setState({
                loading : false,
                text : false,
                success : true
            })
            
            console.log(response);
          })
          .catch(function (error) {
            self.setState({
                error : true,
                success : false,
                loading : false,
                text : false
            })
        });

    }

  render() {
      console.log(cookie.load('token'))
    return (

        <div id="wrapper" class="row wrapper">
        <div class="col-10 ml-sm-auto col-sm-8 col-md-4 ml-md-auto login-center mx-auto">
            <div class="navbar-header text-center mb-5">
                <a href="./index.html">
                    <img alt="" src="./assets/img/header.png" />
                </a>
            </div>

            <h5><a href="javascript:void(0);">Daftar SMMT-BPKAD</a></h5>
            <p>Silahkan isikan data diri anda untuk mulai menggunakan aplikasi.</p>

            {
                this.state.success ? 
                <Animated animationIn="tada" animationOut="zoomOutDown" isVisible={this.state.success}>
                <div class="alert alert-success">
                    Sukses! Akun anda berhasil dibuat, silahkan login terlebih dahulu.
                </div> 
                </Animated>
                : 
                null
            }

            {
                this.state.error ? 
                <Animated animationIn="tada" animationOut="zoomOutDown" isVisible={this.state.error}>
                <div class="alert alert-danger">
                    Gagal ! Mohon maaf, ada masalah saat pendaftaran
                </div>
                </Animated>
                : null
            }

            <form id="loginform" onSubmit={this.fileUploadHandler}>
                <div class="form-group">
                    <label>NIP</label>
                    <input onChange={this.handleNIP} class="form-control" type="text" required="" placeholder="NIP" />
                </div>
                <div class="form-group">
                    <label>Nama</label>
                    <input onChange={this.handleNama} class="form-control" type="text" required="" placeholder="Nama" />
                </div>
                <div class="form-group">
                    <label>Jabatan</label>
                    <select class="form-control" id="l13" onChange={this.handleJabatan}>
                        <option>Pilih Jabatan</option>   
                        <option value="Kepala BPKAD">Kepala BPKAD</option>      
                        <option value="Sekretaris">Sekretaris</option>  
                        <option value="Kasubbag Program">Kasubbag Program</option>
                        <option value="Kasubbag Umum">Kasubbag Umum</option>
                        <option value="Kasubbag Keuangan">Kasubbag Keuangan</option>    
                        <option value="Kepala Bidang Anggaran">Kepala Bidang Anggaran</option>  
                        <option value="Kasubbid Anggaran I">Kasubbid Anggaran I</option>
                        <option value="Kasubbid Anggaran II">Kasubbid Anggaran II</option>
                        <option value="Kasubbid Bina dan Evaluasi Anggaran Kabupaten/Kota">Kasubbid Bina dan Evaluasi Anggaran Kabupaten/Kota</option>
                        <option value="Kepala Bidang Akuntansi dan Pelaporan">Kepala Bidang Akuntansi dan Pelaporan</option>     
                        <option value="Kasubbid Akuntansi I">Kasubbid Akuntansi I</option>
                        <option value="Kasubbid Akuntansi II">Kasubbid Akuntansi II</option>
                        <option value="Kasubbid Pelaporan dan Evaluasi Keuangan Daerah">Kasubbid Pelaporan dan Evaluasi Keuangan Daerah</option>
                        <option value="Kepala Bidang Perbendaharaan">Kepala Bidang Perbendaharaan</option>
                        <option value="Kasubbid Pengelolaan KAS">Kasubbid Pengelolaan KAS</option>
                        <option value="Kasubbid Perbendaharaan I">Kasubbid Perbendaharaan I</option>
                        <option value="Kasubbid Perbendaharaan II">Kasubbid Perbendaharaan II</option>
                        <option value="Kepala Bidang Pengelolaan BMD">Kepala Bidang Pengelolaan BMD</option>
                        <option value="Kasubbid Perencanaan Kebutuhan dan Pengadaan BMD">Kasubbid Perencanaan Kebutuhan dan Pengadaan BMD</option>
                        <option value="Kasubbid Pemeliharaan dan Penghapusan BMD">Kasubbid Pemeliharaan dan Penghapusan BMD</option> 
                        <option value="Kasubbid Penatausahaan dan Pembinaan Aset">Kasubbid Penatausahaan dan Pembinaan Aset</option>
                        <option value="Kepala Unit Pengelola Islamic Center">Kepala Unit Pengelola Islamic Center</option>
                        <option value="Kasubbag Tata Usaha Pengelola Islamic Center">Kasubbag Tata Usaha Pengelola Islamic Center</option>
                        <option value="Kasi Pemeliharaan Sarana dan Prasarana">Kasi Pemeliharaan Sarana dan Prasarana</option>
                        <option value="Kasi Pemanfaatan, Pengembangan Usaha dan Bisnis">Kasi Pemanfaatan, Pengembangan Usaha dan Bisnis</option>
                        <option value="Kepala UPTD Balai Pemanfaatan dan Pengamanan Aset">Kepala UPTD Balai Pemanfaatan dan Pengamanan Aset</option>
                        <option value="Kasubbag Tata Usaha UPTD">Kasubbag Tata Usaha UPTD</option>  
                        <option value="Kasi Pemanfaatan Aset">Kasi Pemanfaatan Aset</option>
                        <option value="Kasi Pengamanan Aset">Kasi Pengamanan Aset</option>
                        <option value="Staf">Staf</option>
                    </select>
                </div> 
                <div class="form-group">
                    <label>Kata Sandi</label>
                    <input onChange={this.handleSandi} class="form-control" type="password" required="" placeholder="Kata Sandi" />
                </div>
                <div class="form-group">
                    <label>Lampirkan Foto</label>
                    <input onChange={this.fileSelectedHandler} class="form-control" type="file" required="" />
                </div>
                
                <div class="form-group text-center no-gutters mb-4">
                    <button class="btn btn-block btn-lg btn-color-scheme text-uppercase fs-12 fw-600" type="submit">{ this.state.text ? "Menyimpan data ..." : "Daftar"}</button>
                    <div className='sweet-loading' style={{'marginLeft' : '48%', 'padding' : 10}}>
                        <PropagateLoader
                        color={'#408FE7'} 
                        loading={this.state.loading} 
                        />
                    </div>
                </div>
                
            </form>
            
            <footer class="col-sm-12 text-center">
                <hr />
                <p>Sudah punya akun ? 
                    <NavLink style={{'padding' : '0px'}} to={"/"} >
                        <a href="#" class="text-primary m-l-5"><b> Masuk</b></a>
                    </NavLink>
                </p>
            </footer>
        </div>
    </div>

    );
  }
}

export default Register;
