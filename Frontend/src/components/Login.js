import React, { Component } from 'react';
import axios from 'axios'
import cookie from 'react-cookies';
import {NavLink} from "react-router-dom";
import '../animate.css';
import {Animated} from "react-animated-css";
import { PropagateLoader } from 'react-spinners';
import  { Redirect } from 'react-router-dom';

class Login extends Component {

    constructor(props){

        super(props)
        this.state = {
          nip       : '',
          password  : '',
          loading   : false,
          success   : true
        }
    
        this.handleChangeNip = this.handleChangeNip.bind(this)
        this.handleChangePassword = this.handleChangePassword.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    
      }
    
      handleChangeNip(event){ this.setState({ nip : event.target.value }) }
      handleChangePassword(event){ this.setState({ password : event.target.value }) }
      handleSubmit(event){
        const self = this
        self.setState({loading : true, success : true})
        event.preventDefault()
        console.log(this.state.nip + this.state.password)
        axios.post('http://localhost:8000/api/auth/signin', {
          nip: this.state.nip,
          password: this.state.password
        })
        .then(function (response) {
          console.log(response.data);

        //   const tomorrow = new Date();
        //   tomorrow.setDate(today.getDate()+1);
          cookie.save('user_id', response.data.user_id, {
            path: '/',
            expires: new Date(Date.now()+2592000)
          })
          cookie.save('role', response.data.role, {
            path: '/',
            expires: new Date(Date.now()+2592000)
          })
          cookie.save('level', response.data.level, {
            path: '/',
            expires: new Date(Date.now()+2592000)
          })
          cookie.save('token', response.data.token, {
            path: '/',
            expires: new Date(Date.now()+2592000)
          })
          setTimeout(() => {
            window.location = "/";
          }, 3500)
        })
        .catch(function (error) {
            self.setState({
                loading : false,
                success : false,
                nip     : '',
                password: ''
            })
          console.log(error);
        });
    
        
      }
    
  render() {
    return (

        <div id="wrapper" class="row wrapper">
        <div class="col-10 ml-sm-auto col-sm-8 col-md-4 ml-md-auto login-center mx-auto">
            <div class="navbar-header text-center mb-5">
                <a href="./index.html">
                    <img alt="" src="./assets/img/header.png" />
                </a>
            </div>

            <h5><a href="javascript:void(0);">Masuk SMMT-BPKAD</a></h5>
            <p>Silahkan isikan data diri anda dengan benar.</p>

            {
                this.state.success ? null :
                <Animated animationIn="tada" animationOut="zoomOutDown" isVisible={this.state.error}>
                <div class="alert alert-danger">
                    Gagal ! NIP atau kata sandi tidak tepat
                </div>
                </Animated>
            }

            <form id="loginform" onSubmit={this.handleSubmit}>
                <div class="form-group">
                    <label>NIP</label>
                    <input onChange={this.handleChangeNip} value={this.state.nip} class="form-control" type="text" required="" placeholder="NIP" />
                </div>
                 
                <div class="form-group">
                    <label>Kata Sandi</label>
                    <input onChange={this.handleChangePassword} value={this.state.password} class="form-control" type="password" required="" placeholder="Kata Sandi" />
                </div>

                <div class="form-group text-center no-gutters mb-4">
                    <button class="btn btn-block btn-lg btn-color-scheme text-uppercase fs-12 fw-600" type="submit">{ this.state.loading ? "Memvalidasi data ..." : "Masuk" }</button>
                    <div className='sweet-loading' style={{'marginLeft' : '48%', 'padding' : 10}}>
                        <PropagateLoader
                        color={'#9013FE'} 
                        loading={this.state.loading} 
                        />
                    </div>
                </div>
                
            </form>
            
            <footer class="col-sm-12 text-center">
                <hr />
                <p>Belum punya akun ? 
                    <NavLink style={{'padding' : '0px'}} to={"/daftar"} >
                        <a href="#" class="text-primary m-l-5"><b> Daftar</b></a>
                    </NavLink>
                </p>
            </footer>
        </div>
    </div>

    );
  }
}

export default Login;
