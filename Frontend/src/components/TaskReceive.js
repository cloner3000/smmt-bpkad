import React, { Component } from 'react';
import axios from 'axios'
import cookie from 'react-cookies';
import { NavLink} from "react-router-dom";

class TaskReceive extends Component {

    constructor(props){
        super(props)

        this.state = {
            detail : []
        }
    }

    componentDidMount(){
        const self = this

        //load task
        axios.get('http://localhost:8000/api/taskreceive/' + cookie.load('user_id'))
        .then(function (response) {
            console.log("Task " + response.data)
            self.setState({detail : response.data})
        })
        .catch(function (error) {
            console.log(error);
        });
  
    }

  render() {
    let number  = 0
    let status  = ''
    let presentase  = 0

    return (

        <main class="main-wrapper clearfix">

        <div class="row page-title clearfix">
            <div class="page-title-left">
                <h6 class="page-title-heading mr-0 mr-r-5">Daftar Tugas Masuk</h6>
                <p class="page-title-description mr-0 d-none d-md-inline-block">Daftar Tugas Anda</p>
            </div>
        </div>

        <div class="widget-list row">
                <div class="widget-holder widget-full-height col-md-12">
                    <div class="widget-bg">
                        
                        <div class="widget-body">
                            
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Judul Tugas</th>
                                            <th>Pemberi Tugas</th>
                                            <th>Status</th>
                                            <th>Tanggal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            this.state.detail.map(function(detail){
                                                number++          
                                                const d = new Date();
                                                let bulan     = ''
                                                let tanggal   = ''
                                                let status    = ''

                                                if(d.getMonth()+1 < 10){
                                                    bulan = "0"+ (d.getMonth()+1);
                                                }
                                                if(d.getDate() < 10){
                                                    tanggal = "0"+ d.getDate();
                                                }else{
                                                    tanggal = d.getDate()
                                                }
                                                
                                                let tgl = d.getFullYear() + "" + bulan + "" + tanggal
                                                
                                                let str = detail.dibuat;
                                                let res = str.substring(0, 10);
                                                let now = res.split('-').join('');
                                                console.log("now " + now + " tgl " + tgl)
                                                if(now == tgl){
                                                    status = <span class="badge bg-danger-contrast">Baru</span>
                                                }else{
                                                    status = ""
                                                }

                                                                   
                                                return(
                                                    <tr>
                                                        <th>{number}</th>
                                                        <td>
                                                        <NavLink  to={"/detail/" + detail.id_task} >
                                                            {detail.judul} {status}
                                                        </NavLink>
                                                        </td>
                                                        <td>{detail.nama}</td>
                                                        <td>{detail.st_judul}</td>
                                                        <td>{detail.dibuat}</td>
                                                    </tr>
                                                )
                                            })
                                        }
                                    </tbody>
                                </table>
                            
                        </div>
                        
                    </div>
                    
                </div>
                
                
                
            </div>
            
      </main>

    );
  }
}

export default TaskReceive;
