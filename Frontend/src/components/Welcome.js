import React, { Component } from 'react';
import {
  Route,
  Switch
} from "react-router-dom";
import { ClipLoader } from 'react-spinners'
import axios from 'axios'
import '../animate.css';
import {Animated} from "react-animated-css";
import cookie from 'react-cookies';

class Welcome extends Component {

    constructor(props){
        super(props)
        this.state = {
            username : '',
            password :'',
            loading  : false,
            error    : false
        }
    }

    handleUsername = (event) => { this.setState({username : event.target.value}) }
    handlePassword = (event)=> { this.setState({password : event.target.value}) }
    handleSubmit = (event) => {
        const self = this
        event.preventDefault()
        self.setState({
            loading : true,
            error   : false
        })
        axios.post('http://localhost:8000/api/admin', 
        {
          username: this.state.username,
          password: this.state.password
        })
        .then(function (response) {
            cookie.save('access', 'True', {
                path: '/',
                expires: new Date(Date.now()+2592000)
            })  
            setTimeout(() => {
                window.location = "/";
            }, 3800)
        })
        .catch(function (error) {
            self.setState({
                loading : false,
                error   : true
            })
          console.log(error);
        });
    }

  render() {
    return (

        <div id="wrapper" class="wrapper">
            <div class="row container-min-full-height">
                <div class="col-lg-12 p-3 login-left">
                    <div class="w-50">
                        <h2 class="mb-4 text-center">Selamat Datang !</h2>
                        <h6 class="mb-4 text-center">Sistem Manajemen Tugas & Aset BPKAD NTB</h6>
                        <form class="text-center" onSubmit={this.handleSubmit}>

                            {
                                this.state.error ?
                                <Animated animationIn="tada" animationOut="zoomOutDown" isVisible={this.state.error}>
                                <div class="alert alert-icon alert-danger border-danger alert-dismissible fade show" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                    </button> <i class="material-icons list-icon">not_interested</i>  <strong>Gagal ! </strong>Mohon Maaf, kredensial yang anda masukkan tidak tepat</div>
                                    </Animated>
                                : null
                            }

                            <div class="form-group">
                                <label class="text-muted" for="example-email">Username</label>
                                <input type="text" class="form-control form-control-line" name="username" onChange={this.handleUsername} />
                            </div>
                            <div class="form-group">
                                <label class="text-muted" for="example-password">Password</label>
                                <input type="password" class="form-control form-control-line" name="password" onChange={this.handlePassword} />
                            </div>
  
                            <div class="form-group mr-b-20">
                                <button class="btn btn-block btn-rounded btn-md btn-color-scheme text-uppercase fw-600 ripple" type="submit">
                                {
                                    this.state.loading ?
                                    <ClipLoader
                                        color={'#FFFFFF'} 
                                        loading={this.state.loading} 
                                    />
                                    : "Masuk"
                                }
                            </button>
                            </div>
                            
                        </form>
                    </div>
                    
                </div>
            </div>
            
        </div>

    );
  }
}

export default Welcome;
