import React, { Component } from 'react';
import cookie from 'react-cookies'
import { GridLoader, BeatLoader, BarLoader } from 'react-spinners'
import Loading from 'react-loading-bar'
import 'react-loading-bar/dist/index.css'
import { NavLink } from "react-router-dom";
import axios from 'axios'

let limit = 3

class Detail extends Component {

    constructor(props) {
        super(props);
        
        this.state = {
            show        : false,
            pemberi     : [],
            penerima    : [],
            st_judul    : '',
            text_judul  : '',
            st_lv1      : '',
            text_lv1    : '',
            st_dlv1     : '',
            text_dlv1   : '',
            st_lv2      : '',
            text_lv2    : '',
            st_dlv2     : '',
            text_dlv2   : '',
            st_lv3      : '',
            text_lv3    : '',
            st_dlv3     : '',
            text_dlv3   : '',
            komentar    : [],
            comments    : '',
            loading     : false,
            selectedFile : null,
            start       : true
        };
    }

    fileSelectedHandler = event => {
        this.setState({
          selectedFile : event.target.files[0]
        })
    }

    fileUploadHandler = (event) => {
        event.preventDefault()
        const self = this;

        self.setState({
            loading : true
        })

        if(this.state.selectedFile == null){
            self.handleSubmitComment(null)
        }else{
            const fd = new FormData()
            fd.append('image', this.state.selectedFile)
            axios.defaults.headers.common['Authorization'] = 'Client-ID af3942dc3f3cd90'

            axios.post('http://localhost:8000/api/auth/signupimage', fd)
            .then(function (response) {
                //console.log(self.state.nama + self.state.nip + self.state.jabatan + self.state.password)
                console.log(response.data.name);
                self.handleSubmitComment(response.data.name)
            })
            .catch(function (error) {
                console.log(error);
            });
            event.preventDefault()
        }
    }

    handleComment = (event) => {
        this.setState({
            comments : event.target.value
        })
    }

    handleSubmitComment = (foto) => {
        const self = this

        if(cookie.load('user_id') == this.state.pemberi.id){
            this.handleSendComment(this.state.penerima.id, foto)
        }else if(cookie.load('user_id') == this.state.penerima.id){
            this.handleSendComment(this.state.pemberi.id, foto)
        }else{
            const tujuan = '#' + this.state.penerima.id.toString() + this.state.pemberi.id.toString() + '#'
            this.handleSendComment(tujuan, foto)
        }
    }

    handleSendComment = (tujuan, foto) =>{
        const self = this
        axios.post('http://localhost:8000/api/comment', {
            id_task             : this.props.match.params.name,
            user_id             : cookie.load('user_id'),
            user_tujuan         : tujuan,
            komentar            : this.state.comments,
            foto_nip            : foto,
            limit               : limit
        })
        .then(function (response) {
            let komentar = Object.assign([], response.data).reverse();
            setTimeout(() => {
                self.setState({
                    komentar : komentar,
                    comments : '',
                    loading : false
                })
            }, 1000)
        })
        .catch(function (error) {

        });
    }

    handleJudul = (kolom) => {
        const self = this
        let status = ''
        if(this.state.pemberi.user_id == cookie.load('user_id')){
            if(this.state.penerima.st_judul == 'Belum dikerjakan') {
                let penerima = Object.assign({}, this.state.penerima);   
                penerima.st_judul = 'Sedang dikerjakan';                       
                this.setState({
                    penerima
                });
                status  = 'Sedang dikerjakan'
            }else if(this.state.penerima.st_judul == 'Sedang dikerjakan') {
                let penerima = Object.assign({}, this.state.penerima);   
                penerima.st_judul = 'Selesai';    
                this.setState({                   
                    penerima
                });
                status  = 'Selesai'
            }else if (this.state.penerima.st_judul == 'Selesai') {
                let penerima = Object.assign({}, this.state.penerima);   
                penerima.st_judul = 'Belum dikerjakan';                       
                this.setState({
                    penerima
                });
                status  = 'Belum dikerjakan'
            }
            this.handleChangeStatus(kolom, status)
        }

    }

    handleLv1 = (kolom) => {
        
        const self = this
        let status = ''
        
        if(this.state.pemberi.user_id == cookie.load('user_id')){
            if(this.state.penerima.st_lv1 == 'Belum dikerjakan') {
                let penerima = Object.assign({}, this.state.penerima);   
                penerima.st_lv1 = 'Sedang dikerjakan';                       
                this.setState({
                    penerima
                });
                status  = 'Sedang dikerjakan'
            }else if(this.state.penerima.st_lv1 == 'Sedang dikerjakan') {
                let penerima = Object.assign({}, this.state.penerima);   
                penerima.st_lv1 = 'Selesai';    
                this.setState({                   
                    penerima
                });
                status  = 'Selesai'
            }else if (this.state.penerima.st_lv1 == 'Selesai') {
                let penerima = Object.assign({}, this.state.penerima);   
                penerima.st_lv1 = 'Belum dikerjakan';                       
                this.setState({
                    penerima
                });
                status  = 'Belum dikerjakan'
            }
            this.handleChangeStatus(kolom, status)
        }
    }

    handleLv2 = (kolom) => {
        const self = this
        let status = ''
        
        if(this.state.pemberi.user_id == cookie.load('user_id')){
            if(this.state.penerima.st_lv2 == 'Belum dikerjakan') {
                let penerima = Object.assign({}, this.state.penerima);   
                penerima.st_lv2 = 'Sedang dikerjakan';                       
                this.setState({
                    penerima
                });
                status  = 'Sedang dikerjakan'
            }else if(this.state.penerima.st_lv2 == 'Sedang dikerjakan') {
                let penerima = Object.assign({}, this.state.penerima);   
                penerima.st_lv2 = 'Selesai';    
                this.setState({                   
                    penerima
                });
                status  = 'Selesai'
            }else if (this.state.penerima.st_lv2 == 'Selesai') {
                let penerima = Object.assign({}, this.state.penerima);   
                penerima.st_lv2 = 'Belum dikerjakan';                       
                this.setState({
                    penerima
                });
                status  = 'Belum dikerjakan'
            }
            this.handleChangeStatus(kolom, status)
        }
    }

    handleLv3 = (kolom) => {
        const self = this
        let status = ''

        if(this.state.pemberi.user_id == cookie.load('user_id')){
            if(this.state.penerima.st_lv3 == 'Belum dikerjakan') {
                let penerima = Object.assign({}, this.state.penerima);   
                penerima.st_lv3 = 'Sedang dikerjakan';                       
                this.setState({
                    penerima
                });
                status  = 'Sedang dikerjakan'
            }else if(this.state.penerima.st_lv3 == 'Sedang dikerjakan') {
                let penerima = Object.assign({}, this.state.penerima);   
                penerima.st_lv3 = 'Selesai';    
                this.setState({                   
                    penerima
                });
                status  = 'Selesai'
            }else if (this.state.penerima.st_lv3 == 'Selesai') {
                let penerima = Object.assign({}, this.state.penerima);   
                penerima.st_lv3 = 'Belum dikerjakan';                       
                this.setState({
                    penerima
                });
                status  = 'Belum dikerjakan'
            }
            this.handleChangeStatus(kolom, status)
        }
    }

    handleDlv1 = (kolom) => {
        const self = this
        let status = ''

        if(this.state.pemberi.user_id == cookie.load('user_id')){
            if(this.state.penerima.st_d_lv1 == 'Belum dikerjakan') {
                let penerima = Object.assign({}, this.state.penerima);   
                penerima.st_d_lv1 = 'Sedang dikerjakan';                       
                this.setState({
                    penerima
                });
                status  = 'Sedang dikerjakan'
            }else if(this.state.penerima.st_d_lv1 == 'Sedang dikerjakan') {
                let penerima = Object.assign({}, this.state.penerima);   
                penerima.st_d_lv1 = 'Selesai';    
                this.setState({                   
                    penerima
                });
                status  = 'Selesai'
            }else if (this.state.penerima.st_d_lv1 == 'Selesai') {
                let penerima = Object.assign({}, this.state.penerima);   
                penerima.st_d_lv1 = 'Belum dikerjakan';                       
                this.setState({
                    penerima
                });
                status  = 'Belum dikerjakan'
            }
            this.handleChangeStatus(kolom, status)
        }
    }

    handleDlv2 = (kolom) => {
        const self = this
        let status = ''

        if(this.state.pemberi.user_id == cookie.load('user_id')){
            if(this.state.penerima.st_d_lv2 == 'Belum dikerjakan') {
                let penerima = Object.assign({}, this.state.penerima);   
                penerima.st_d_lv2 = 'Sedang dikerjakan';                       
                this.setState({
                    penerima
                });
                status  = 'Sedang dikerjakan'
            }else if(this.state.penerima.st_d_lv2 == 'Sedang dikerjakan') {
                let penerima = Object.assign({}, this.state.penerima);   
                penerima.st_d_lv2 = 'Selesai';    
                this.setState({                   
                    penerima
                });
                status  = 'Selesai'
            }else if (this.state.penerima.st_d_lv2 == 'Selesai') {
                let penerima = Object.assign({}, this.state.penerima);   
                penerima.st_d_lv2 = 'Belum dikerjakan';                       
                this.setState({
                    penerima
                });
                status  = 'Belum dikerjakan'
            }
            this.handleChangeStatus(kolom, status)
        }
    }

    handleDlv3 = (kolom) => {
        const self = this
        let status = ''

        if(this.state.pemberi.user_id == cookie.load('user_id')){
            if(this.state.penerima.st_d_lv3 == 'Belum dikerjakan') {
                let penerima = Object.assign({}, this.state.penerima);   
                penerima.st_d_lv3 = 'Sedang dikerjakan';                       
                this.setState({
                    penerima
                });
                status  = 'Sedang dikerjakan'
            }else if(this.state.penerima.st_d_lv3 == 'Sedang dikerjakan') {
                let penerima = Object.assign({}, this.state.penerima);   
                penerima.st_d_lv3 = 'Selesai';    
                this.setState({                   
                    penerima
                });
                status  = 'Selesai'
            }else if (this.state.penerima.st_d_lv3 == 'Selesai') {
                let penerima = Object.assign({}, this.state.penerima);   
                penerima.st_d_lv3 = 'Belum dikerjakan';                       
                this.setState({
                    penerima
                });
                status  = 'Belum dikerjakan'
            }
            this.handleChangeStatus(kolom, status)
        }
    }

    handleChangeStatus(kolom, status){
        const self = this
        //load task
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + cookie.load('token')
        axios.put('http://localhost:8000/api/task/' + kolom + "/" + self.props.match.params.name, {
           ubah : status
        })
        .then(function (response) {
            //console.log(response.data)
        })
        .catch(function (error) {
            console.log(error);
        });
        
       // this.handleChangeJudul()
        
    }    
  
    componentDidMount(){
        const self = this
        setTimeout(() => {
            this.setState({show:true})
            setTimeout(() => {
                this.setState({show:false})
            }, 200)
            }, 100)
            
        //load task
        axios.get('http://localhost:8000/api/detail/penerima/' + self.props.match.params.name)
        .then(function (response) {
            self.setState({penerima : response.data[0]})
        })
        .catch(function (error) {
            console.log(error);
        });

        //load komentar
        axios.get('http://localhost:8000/api/comment/' + self.props.match.params.name + '/' + limit)
        .then(function (response) {
            let komentar = Object.assign([], response.data).reverse();
            self.setState({komentar : komentar})
        })
        .catch(function (error) {
            console.log(error);
        });
    
        //load task
        axios.get('http://localhost:8000/api/detail/pemberi/' + self.props.match.params.name)
        .then(function (response) {
            
            self.setState({
                pemberi : response.data[0],
                start : false
            })
        })
        .catch(function (error) {
            console.log(error);
        });
  
    }

    handleChangeJudul = (lv1, lv2, lv3) => {
        const self = this
        let status = ''
        console.log("lv1 " + lv1)
        if(lv1 == 'Sedang dikerjakan' || lv2 == 'Sedang dikerjakan' || lv3 == 'Sedang dikerjakan'){
            let penerima = Object.assign({}, this.state.penerima);   
            penerima.st_judul = 'Sedang dikerjakan';                       
            this.setState({
                penerima
            });
            status = "Sedang dikerjakan"
        }else if(this.state.penerima.st_lv1 == 'Belum dikerjakan' && this.state.penerima.st_lv2 == 'Belum dikerjakan' && this.state.penerima.st_lv3 == 'Belum dikerjakan'){
            status = "Belum dikerjakan"
        }else if(this.state.penerima.st_lv1 == 'Selesai' && this.state.penerima.st_lv2 == 'Selesai' && this.state.penerima.st_lv3 == 'Selesai'){
            status = "Selesai"
        }

        axios.defaults.headers.common['Authorization'] = 'Bearer ' + cookie.load('token')
        axios.put('http://localhost:8000/api/task/' + "st_judul" + "/" + self.props.match.params.name, {
           ubah : status
        })
        .then(function (response) {
            console.log(response.data)
        })
        .catch(function (error) {
            console.log(error);
        });

    }

    handleLoadMore = () => {
        limit += 3
        const self = this
        //load komentar
        axios.get('http://localhost:8000/api/comment/' + self.props.match.params.name + '/' + limit)
        .then(function (response) {
            let komentar = Object.assign([], response.data).reverse();
            self.setState({komentar : komentar})
        })
        .catch(function (error) {
            console.log(error);
        });
    }

  render() {
      
    let check = false
    if(this.state.komentar != null) check = true
    
    let st_judul        = ''
    let {text_judul}    = ''
    
    if(this.state.penerima.st_judul == "Belum dikerjakan") {
        st_judul = "list-icon fa fa-stop-circle color-danger"
        text_judul = "box-title text-danger"
    } else if(this.state.penerima.st_judul == "Sedang dikerjakan"){
        st_judul = "list-icon fa fa-play-circle color-warning"
        text_judul = "box-title text-warning"
    }else {
        st_judul = "list-icon fa fa-check-circle color-success"
        text_judul = "box-title text-success"
    }

    let {st_lv1}      = this.state
    let {text_lv1}    = this.state
    if(this.state.penerima.st_lv1 == "Belum dikerjakan") {
        st_lv1          = "list-icon fa fa-stop-circle color-danger"
        text_lv1    = "text-danger"
    } else if(this.state.penerima.st_lv1 == "Sedang dikerjakan"){
        st_lv1 = "list-icon fa fa-play-circle color-warning"
        text_lv1 = "text-warning"
    }else {
        st_lv1 = "list-icon fa fa-check-circle color-success"
        text_lv1 = "text-success"
    }

    let {st_dlv1}     = this.state
    let {text_dlv1}   = this.state
    if(this.state.penerima.st_d_lv1 == "Belum dikerjakan") {
        st_dlv1          = "list-icon fa fa-stop-circle color-danger"
        text_dlv1    = "text-danger"
    } else if(this.state.penerima.st_d_lv1 == "Sedang dikerjakan"){
        st_dlv1 = "list-icon fa fa-play-circle color-warning"
        text_dlv1 = "text-warning"
    }else {
        st_dlv1 = "list-icon fa fa-check-circle color-success"
        text_dlv1 = "text-success"
    }

    let {st_lv2}      = this.state
    let {text_lv2}    = this.state
    if(this.state.penerima.st_lv2 == "Belum dikerjakan") {
        st_lv2      = "list-icon fa fa-stop-circle color-danger"
        text_lv2    = "text-danger"
    } else if(this.state.penerima.st_lv2 == "Sedang dikerjakan"){
        st_lv2      = "list-icon fa fa-play-circle color-warning"
        text_lv2    = "text-warning"
    }else {
        st_lv2      = "list-icon fa fa-check-circle color-success"
        text_lv2    = "text-success"
    }

    let {st_dlv2}     = this.state
    let {text_dlv2}   = this.state
    if(this.state.penerima.st_d_lv2 == "Belum dikerjakan") {
        st_dlv2          = "list-icon fa fa-stop-circle color-danger"
        text_dlv2    = "text-danger"
    } else if(this.state.penerima.st_d_lv2 == "Sedang dikerjakan"){
        st_dlv2 = "list-icon fa fa-play-circle color-warning"
        text_dlv2 = "text-warning"
    }else {
        st_dlv2 = "list-icon fa fa-check-circle color-success"
        text_dlv2 = "text-success"
    }

    let {st_lv3}      = this.state
    let {text_lv3}    = this.state
    if(this.state.penerima.st_lv3 == "Belum dikerjakan") {
        st_lv3      = "list-icon fa fa-stop-circle color-danger"
        text_lv3    = "text-danger"
    } else if(this.state.penerima.st_lv3 == "Sedang dikerjakan"){
        st_lv3      = "list-icon fa fa-play-circle color-warning"
        text_lv3    = "text-warning"
    }else {
        st_lv3      = "list-icon fa fa-check-circle color-success"
        text_lv3    = "text-success"
    }

    let {st_dlv3}     = this.state
    let {text_dlv3}   = this.state
    if(this.state.penerima.st_d_lv3 == "Belum dikerjakan") {
        st_dlv3          = "list-icon fa fa-stop-circle color-danger"
        text_dlv3    = "text-danger"
    } else if(this.state.penerima.st_d_lv3 == "Sedang dikerjakan"){
        st_dlv3 = "list-icon fa fa-play-circle color-warning"
        text_dlv3 = "text-warning"
    }else {
        st_dlv3 = "list-icon fa fa-check-circle color-success"
        text_dlv3 = "text-success"
    }

    let comment = false
    if(this.state.komentar != null) {comment = true}

    const self = this

    return (

        <main class="main-wrapper clearfix">

        <div class="row page-title clearfix">
            <div class="page-title-left">
                <h6 class="page-title-heading mr-0 mr-r-5">Detail Tugas</h6>
                <p class="page-title-description mr-0 d-none d-md-inline-block">Rincian & Status Tugas</p>
            </div>
        </div>

        <div class="widget-list row">
                <div class="widget-holder widget-full-height col-md-12">
                    <div class="widget-bg">
                        <div class="widget-heading widget-heading-border">
                            <table class="widget-holder widget-full-height col-md-12" style={{'margin' : 10}}>
                                <tr>
                                    <td>
                                        <figure class="single-user-avatar  thumb-xs2">
                                            <a href="#">
                                                <img class="rounded-circle" src={this.state.pemberi.foto}  />
                                            </a>
                                        </figure>
                                        <NavLink  to={"/profile/" + this.state.penerima.user_id} >
                                        <a href="#" class="badge bg-info-contrast px-3 cursor-pointer heading-font-family">{this.state.pemberi.nama} <br/>( {this.state.pemberi.jabatan} )</a>
                                        </NavLink>
                                    </td>
                                    <td>
                                        Kepada
                                    </td>
                                    <td class="pull-right">
                                        <figure class="single-user-avatar  thumb-xs2">
                                            <a href="#">
                                                <img class="rounded-circle" src={this.state.penerima.foto}  />
                                            </a>
                                        </figure>
                                        <NavLink  to={"/profile/" + this.state.pemberi.nip_tujuan} >
                                        <a href="#" class="badge bg-info-contrast px-3 cursor-pointer heading-font-family">{this.state.penerima.nama} <br/>( {this.state.penerima.jabatan} )</a>
                                        </NavLink>
                                    </td>
                                </tr>
                            </table>
                            
                        </div>
                        
                        {
                            this.state.start ?
                            <div style={{'marginLeft' : '45%'}}>
                                <GridLoader
                                    color={'#9013FE'} 
                                    loading={this.state.start}
                                />
                            </div>
                            :
                            <div class="widget-body row">
                            
                                <div class="widget-holder col-md-7 sitemap-right sitemap mr-b-40">
                                <div class="widget-bg">
                                    <div class="widget-heading widget-heading-border">
                                        <h5 class="widget-title">Rincian Tugas</h5>
                                    </div>
                                    <div class="widget-body">
                                    <div>

                                    </div>
                                    <h5 class={text_judul}>{this.state.penerima.judul} <i onClick={() => this.handleJudul("st_judul")} class={st_judul}></i> </h5>
                                                
                                                <ul class="sitemap-list sitemap-list-icons list-unstyled">
                                                    <li class="menu-item-has-children"><a ><strong class={text_lv1}>{this.state.penerima.lv1} </strong><i onClick={() => this.handleLv1("st_lv1")} class={st_lv1}></i></a>
                                                        <br/>
                                                        {
                                                            this.state.penerima.d_lv1 == null ?  null : 
                                                        
                                                            <ul class="sub-menu list-unstyled">
                                                                <li><a  class={text_dlv1}>{this.state.penerima.d_lv1} </a> <i onClick={() => this.handleDlv1("st_d_lv1")} class={st_dlv1}></i>
                                                                </li>
                                                            </ul>
                                                        }
                                                    </li>
                                                    <br/>
                                                    <li class="menu-item-has-children"><a ><strong class={text_lv2}>{this.state.penerima.lv2}</strong></a> <i onClick={() => this.handleLv2("st_lv2")} class={st_lv2}></i>
                                                        <br/>
                                                        {
                                                            this.state.penerima.d_lv2 == null ?  null :
                                                        
                                                            <ul class="sub-menu list-unstyled">
                                                                <li><a class={text_dlv2}>{this.state.penerima.d_lv2}</a> <i onClick={() => this.handleDlv2("st_d_lv2")} class={st_dlv2}></i>
                                                                </li>
                                                            </ul>
                                                        }
                                                    </li>
                                                    <br/>
                                                    <li class="menu-item-has-children"><a><strong class={text_lv3}>{this.state.penerima.lv3}</strong></a> <i onClick={() => this.handleLv3("st_lv3")} class={st_lv3}></i>
                                                        <br/>
                                                        {
                                                            this.state.penerima.d_lv3 == null ?  null :
                                                            <ul class="sub-menu list-unstyled">
                                                                <li><a class={text_dlv3}>{this.state.penerima.d_lv3}</a> <i onClick={() => this.handleDlv3("st_d_lv3")} class={st_dlv3}></i>
                                                                </li>
                                                            </ul>
                                                        }
                                                        
                                                    </li>
                                                </ul>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="widget-holder col-sm-5">
                                    <div class="widget-bg">
                                        <div class="widget-heading widget-heading-border">
                                            <h5 class="widget-title">Komentar Tugas</h5>
                                        </div>
                                        
                                        <div class="widget-body">
                                            <div class="widget-latest-comments">

                                                {
                                                    this.state.komentar != '' ?
                                                    <div onClick={this.handleLoadMore} class="mr-b-20">
                                                        <button class="btn btn-block btn-md btn-outline-default ripple">Komentar Sebelumnya</button>
                                                    </div>
                                                    : null
                                                }

                                                {
                                                    this.state.komentar.map(function(x){
                                                        if(x == undefined) return( <h5>Belum ada komentar </h5> )
                                                        else {
                                                            return(
                                                                <div class="single media">
                                                                    <figure class="single-user-avatar thumb-xs2">
                                                                        <a href="#">
                                                                            <img class="rounded-circle" src={x.foto} />
                                                                        </a>
                                                                    </figure>
                                                                    <div class="media-body">
                                                                    <NavLink  to={"/profile/" + x.user_id} >
                                                                        <div class="single-header"><a href="#" class="single-user-name">{x.nama}</a>  <span class="single-timestamp text-muted">{x.created_at}</span>
                                                                        </div>      
                                                                    </NavLink>   

                                                                    {
                                                                        x.foto_nip != null ?
                                                                        <div class="single-gallery lightbox-gallery row" data-toggle="lightbox-gallery" data-type="image">
                                                                        <div class="lightbox col-7">
                                                                            <a href="#">
                                                                                <img onClick={self.handleOpen} src={"http://localhost:8000/images/"+x.foto_nip} width="10" />
                                                                            </a>
                                                                        </div>
                                                                    </div> : null   
                                                                    }

                                                                    <br/>      
                                                                        <p>{x.komentar}</p>
                                                                    </div>   
                                                                </div>
                                                            )
                                                        }
                                                    })
                                                }
                                            </div>

                                            <form class="mr-t-10" onSubmit={this.fileUploadHandler}>
                                                <div class="form-group row">
                                                    <div class="col-md-10">
                                                        <input onChange={this.handleComment} value={this.state.comments} class="form-control" id="l30" placeholder="Komentar anda" type="text" />
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="btn btn-success">
                                                            Foto
                                                            <input onChange={this.fileSelectedHandler} class="form-control" id="l8" onChange={this.fileSelectedHandler} type="file" style={{'display' : 'none'}}  />
                                                        </label>
                                                    </div>
                                                </div>

                                                {/* <div class="form-group row">
                                                    <label class="col-md-12 col-form-label" for="l16"></label>
                                                    <div class="col-md-9">
                                                        <input onChange={this.fileSelectedHandler} id="l16" type="file"  />
                                                        <br /><small class="text-muted">Lampirkan gambar</small>
                                                    </div>
                                                </div> */}
                                                <div onClick={this.fileUploadHandler} class="mr-b-20">
                                                    <button class="btn btn-block btn-md btn-outline-primary ripple">
                                                        {
                                                            this.state.loading ?
                                                            <BarLoader
                                                                color={'#9013FE'} 
                                                                width={320}
                                                                height={6}
                                                                loading={this.state.loading} 
                                                            />
                                                            :
                                                            "Kirim"
                                                        }
                                                    </button>
                                                </div>
                                            </form>
                                            
                                        </div>
                                        
                                    </div>
                            
                                </div>
                                    
                                </div>
                        }
                        
                    </div>
                    
                </div>
                
                
                
            </div>
            
      </main>

    );
  }
}

export default Detail;
