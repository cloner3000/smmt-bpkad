<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Admin;

class AdminController extends Controller
{
    public function index()
    {
    	$data = DB::select( DB::raw("SELECT * FROM admin") );
        return response()->json($data, 200);
    }

    public function update(Request $request)
    {
    	$username = $request->json('username');
    	$password = $request->json('password');

		DB::table('admin')
            ->update(['username' => $username, 'password' => $password]);
		return DB::select( DB::raw("SELECT * FROM admin") );
    }

    public function auth(Request $request)
    {
        $username = $request->json('username');
        $password = $request->json('password');

        $data = DB::select( DB::raw("SELECT * FROM admin WHERE username = '$username' AND password = '$password'") );
        
        if ($data) {
            return response()->json(['message' => 'success'], 200);
        }else{
            return response()->json(['message' => 'error'], 403);
        }
    }
}
