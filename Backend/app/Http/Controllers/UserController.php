<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    
    public function index()
    {
    	$data = DB::select( DB::raw("SELECT * FROM users ORDER BY nama") );
        return $data;
        //return User::all();
    }

    public function show(Request $request)
    {

    	return $request->user();

    }

    public function showUser($id)
    {
    	$data = DB::select( DB::raw("SELECT * FROM users WHERE id = $id") );
        return $data;
    }

    public function usersbylevel($level)
    {
        $data = DB::select( DB::raw("SELECT * FROM users WHERE level <= $level") );
        return $data;
    }

    public function destroy($id)
    {
        DB::select( DB::raw("DELETE FROM users WHERE id = $id") );
        $data = DB::select( DB::raw("SELECT * FROM users ORDER BY nama") );
        return $data;
    }

    public function updateImage(Request $request, $id)
    {
        $foto = $request->json('foto');
        var_dump($foto);

        DB::select( DB::raw("UPDATE users SET foto = '$foto' WHERE id = $id") );
        $this->showUser($id);
    }

    public function update(Request $request, $id)
    {

        $nip        = $request->json('nip');
        $nama       = $request->json('nama');
        $jabatan    = $request->json('jabatan');

        $level = 0;

        if ($request->json('jabatan') === "Staf") {
            $level = 0;
        }else if ($request->json('jabatan') === "Kasi Pemanfaatan Aset" || 
                $request->json('jabatan') === "Kasi Pengamanan Aset" || 
                $request->json('jabatan') === "Kasubbag Tata Usaha UPTD" ||
                $request->json('jabatan') === "Kasubbag Tata Usaha Pengelola Islamic Center" ||
                $request->json('jabatan') === "Kasi Pemeliharaan Sarana dan Prasarana" ||
                $request->json('jabatan') === "Kasi Pemanfaatan, Pengembangan Usaha dan Bisnis" ||
                $request->json('jabatan') === "Kasubbid Perencanaan Kebutuhan dan Pengadaan BMD" ||
                $request->json('jabatan') === "Kasubbid Pemeliharaan dan Penghapusan BMD" ||
                $request->json('jabatan') === "Kasubbid Penatausahaan dan Pembinaan Aset" ||
                $request->json('jabatan') === "Kasubbid Perbendaharaan II" ||
                $request->json('jabatan') === "Kasubbid Perbendaharaan I" ||
                $request->json('jabatan') === "Kasubbid Pengelolaan KAS" ||
                $request->json('jabatan') === "Kasubbid Pelaporan dan Evaluasi Keuangan Daerah" ||
                $request->json('jabatan') === "Kasubbid Akuntansi II" ||
                $request->json('jabatan') === "Kasubbid Akuntansi I" ||
                $request->json('jabatan') === "Kasubbid Bina dan Evaluasi Anggaran Kabupaten/Kota" ||
                $request->json('jabatan') === "Kasubbid Anggaran II" ||
                $request->json('jabatan') === "Kasubbid Anggaran I" ||
                $request->json('jabatan') === "Kasubbag Keuangan" ||
                $request->json('jabatan') === "Kasubbag Umum" ||
                $request->json('jabatan') === "Kasubbag Program" ) {
            $level = 1;
        }else if ($request->json('jabatan') === "Sekretaris" || 
                $request->json('jabatan') === "Kepala Bidang Anggaran" ||
                $request->json('jabatan') === "Kepala Bidang Akuntansi dan Pelaporan" ||
                $request->json('jabatan') === "Kepala Bidang Perbendaharaan" ||
                $request->json('jabatan') === "Kepala Bidang Pengelolaan BMD" ||
                $request->json('jabatan') === "Kepala Unit Pengelola Islamic Center" ||
                $request->json('jabatan') === "Kepala UPTD Balai Pemanfaatan dan Pengamanan Aset") {
            $level = 2;
        }else if ($request->json('jabatan') === "Kepala BPKAD" ) {
            $level = 3;
        }

        if ($request->json('password') == '') {
            DB::select( DB::raw("UPDATE users SET nip = '$nip', 
                nama = '$nama', jabatan = '$jabatan', level = $level WHERE id = $id") );
            $data = DB::select( DB::raw("SELECT * FROM users ORDER BY nama") );
            return response()->json([$data, 'message' => 'success'], 200);           
        }else{

            $password = bcrypt($request->json('password'));

            DB::select( DB::raw("UPDATE users SET nip = '$nip', 
                nama = '$nama', jabatan = '$jabatan', level = $level, password = '$password' WHERE id = $id") );
            $data = DB::select( DB::raw("SELECT * FROM users ORDER BY nama") );
            return response()->json([$data, 'message' => 'success'], 200); 
        }

    }


}
