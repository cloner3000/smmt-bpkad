<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Comment extends Model
{
    protected $fillable = [
		'id_task',
		'user_id',
		'user_tujuan',
		'komentar',
		'foto_komentar',
		'foto_nip'
    ];
}
