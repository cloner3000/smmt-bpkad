<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nip_asal', 20);
            $table->string('nip_tujuan',20);
            $table->text('judul');
            $table->text('detail1');
            $table->text('detail2');
            $table->text('detail3');
            $table->string('status_judul', 20);
            $table->string('status_detail1', 20);
            $table->string('status_detail2', 20);
            $table->string('status_detail3', 20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
